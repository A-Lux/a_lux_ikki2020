<div class="footer">
    <div class="container mt-5">
        <div class="footer__separator">
            <div class="row">
                <div class="col-lg-3">
                    <div class="d-flex flex-column">
                        <h2>о компании</h2>
                        <a href="about.php">О нас</a>
                        <a href="comments.php">Отзывы</a>
                        <a href="shops.php">Магазины</a>
                        <a href="contacts.php">Контакты</a>
                        <a href="vacancy.php">Вакансии</a>
                    </div>
                </div>
                <div class="offset-lg-2"></div>
                <div class="col-lg-3">
                    <div class="d-flex flex-column">
                        <h2>покупателям</h2>
                        <a href="#">Оформление заказа</a>
                        <a href="delivery.php">Доставка и оплата</a>
                        <a href="articles.php">Справочник размеров</a>
                        <a href="exchange.php">Возврат</a>
                        <a href="questions.php">Вопросы и ответы</a>
                    </div>
                </div>
                <div class="offset-lg-1"></div>
                <div class="col-lg-3">
                    <div class="d-flex flex-column footer__subscription">
                        <h2>УЗНАЙТЕ ПЕРВЫМИ О НОВИНКАХ И СКИДКАХ</h2>
                        <form action="" method="POST" class="subscription__form">
                            <input type="email" name="email" placeholder="Укажите E-mail">
                            <button class="subscription__button" type="submit">Подписаться</button>
                        </form>
                        <p>Нажимая на кнопку «Подписаться», я соглашаюсь на обработку моих персональных данных и ознакомлен(а) с <a href="#">условиями конфиденциальности.</a></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3">
                    <p>(с) IKKI 2019</p>
                </div>
                <div class="offset-lg-2"></div>
                <div class="col-lg-2">
                    <div class="d-flex flex-row footer__socials">
                        <a href="#">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                        <a href="#">
                            <i class="fab fa-instagram"></i>
                        </a>
                        <a href="#">
                            <i class="fab fa-youtube"></i>
                        </a>
                        <a href="#">
                            <i class="fab fa-whatsapp"></i>
                        </a>
                        <a href="#">
                            <i class="fab fa-pinterest-p"></i>
                        </a>
                    </div>
                </div>
                <div class="offset-lg-2"></div>
                <div class="col-lg-3"></div>
            </div>
        </div>
    </div>
</div>
<script src="./js/main.js"></script>
</body>
</html>