<?php require_once("header.php"); ?>

<div class="contacts">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="bread__crumbs">
                    <a href="#" class='crumb'>главная</a><span>мгазины</span>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="contacts__title">
                    Магазины
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8">
                <div class="map">
                    <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A5eaf95487ea05170fc4f959bc33fd2c49d4e306125143c2ce9ce4535b119db05&amp;source=constructor" width="100%" height="400" frameborder="0"></iframe>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="contacts__column--wrapper">
                    <div class="contacts__column--wrapper__item">
                        <h2>
                            Наш Адрес
                        </h2>
                        <p><span>Казахстан, Г. Алма-Ата, Набережная 19</span></p>
                    </div>
                    <div class="contacts__column--wrapper__item">
                        <h2>
                            Наш телефон
                        </h2>
                        <p><span> <a href=""> 8 800 321 50 14 </a>/ <a href="">+7 (961) 525 79 31</a></span></p>
                    </div>
                    <div class="contacts__column--wrapper__item">
                        <h2>
                            Наша почта
                        </h2>
                        <a href="">ikkiinfo@mail.com</a>
                    </div>
                </div>
                <a href="#" data-toggle="modal" data-target="#exampleModalCenter" class=" contacts__btn-feddback">Написать нам</a>
                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">

                            <div class="modal-body">
                                <input type="text" placeholder="ФИО">
                                <input type="text" placeholder="Email">
                                <input type="text" placeholder="Телефон">
                                <textarea name="" id="" cols="30" rows="5"></textarea>
                            </div>
                            <div class="modal-footer">

                                <div class="col-sm-6">
                                    <button class="contacts__btn-feddback contacts__btn-feddback--modal" data-dismiss="modal">Закрыть</button>
                                </div>
                                <div class="col-sm-6">
                                    <button class="contacts__btn-feddback contacts__btn-feddback--modal">Отправить</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require_once("slider.php"); ?>
<?php require_once("footer.php"); ?>