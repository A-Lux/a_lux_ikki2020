<?php require_once("header.php"); ?>
<div class="catalog">
    <div class="container">
        <div class="row mt-5 mb-5">
            <div class="col-lg-12">
                <div class="bread__crumbs">
                    <a href="#" class='crumb'>главная</a><span>каталог</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="catalog__title--wrapper">
                    <h1 class="catalog__title">Каталог</h1>
                    <div class="catalog__quantity">
                        <p>Показывать по:</p>
                        <button type="button" class="quantity-target active-page">50</span>
                            <button type="button" class="quantity-target">100</span>
                                <button type="button" class="quantity-target">200</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-lg-3">
                <div class="catalog__body-container_collapse">
                    <div class="accordion" id="accordion">
                        <div class="catalog__card">
                            <div class="catalog__card-header" id="headingOne">
                                <h2 class="mb-0">
                                    <button class="btn catalog__btn" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        ОДЕЖДА
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="catalog__card-body">
                                    <ul>
                                        <li>Блузы и рубашки</li>
                                        <li>Боди</li>
                                        <li>Брюки</li>
                                        <li>Верхняя одежда</li>
                                        <li>Джемперы, свитеры</li>
                                        <li>Джинсы</li>
                                        <li>Домашняя одежда</li>
                                        <li>Комбинезоны</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="catalog__card">
                            <div class="catalog__card-header" id="headingTwo">
                                <h2 class="mb-0">
                                    <button class="btn catalog__btn collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        ОБУВЬ
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="catalog__card-body">
                                    <ul>
                                        <li>Блузы и рубашки</li>
                                        <li>Боди</li>
                                        <li>Брюки</li>
                                        <li>Верхняя одежда</li>
                                        <li>Джемперы, свитеры</li>
                                        <li>Джинсы</li>
                                        <li>Домашняя одежда</li>
                                        <li>Комбинезоны</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="catalog__card">
                            <div class="catalog__card-header" id="headingThree">
                                <h2 class="mb-0">
                                    <button class="btn catalog__btn collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        АКСЕССУАРЫ
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="catalog__card-body">
                                    <ul>
                                        <li>Блузы и рубашки</li>
                                        <li>Боди</li>
                                        <li>Брюки</li>
                                        <li>Верхняя одежда</li>
                                        <li>Джемперы, свитеры</li>
                                        <li>Джинсы</li>
                                        <li>Домашняя одежда</li>
                                        <li>Комбинезоны</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-6 pl-0">
                        <div class="catalog__body-container_filter">
                            <div class="fiter-category">
                                <button type="button" class="filter__btns by__price-btn" id="priceBtn">
                                    по цене
                                </button>
                                <div class="fiter-caterory-content text-center pb-3">
                                    <form action="">
                                        <label for="">
                                            От
                                            <input type="text"
                                            
                                            placeholder="0 KZT" id="from" onkeyup = 'this.value=parseInt(this.value) | 0' value="0">
                                        </label>
                                        <label for="">
                                            До
                                            <input type="text"
                                            
                                            placeholder="30050 KZT" onkeyup = 'this.value=parseInt(this.value) | 0' id="before" value="0">
                                        </label>
                                    </form>

                                    <button class="btn btn-light p-1" id="searchBtn">Найти</button>
                                </div>
                            </div>
                            <div class="filter-color">
                                <button type="button" class="filter__btns by__color-btn">
                                    по цвету
                                </button>
                                <div class="fiter-color-content">
                                    <!-- <span><i class="fad fa-circle" style="color: #7c2d1c;"></i></span>
                                    <span><i class="fad fa-circle" style="color: #adc2a3;"></i></span>
                                    <span><i class="fad fa-circle" style="color: #fff;"></i></span>
                                    <span><i class="fad fa-circle" style="color: #c55d6a;"></i></span>
                                    <span><i class="fad fa-circle" style="color: #b1c5cc;"></i></span>
                                    <span><i class="fad fa-circle" style="color: #000;"></i></span>
                                    <span><i class="fad fa-circle" style="color: #c4b49d;"></i></span>
                                    <span><i class="fad fa-circle" style="color: #efccb9;"></i></span>
                                    <span><i class="fad fa-circle" style="color: #94c1b0;"></i></span>
                                    <span><i class="fad fa-circle" style="color: #a1738f;"></i></span>
                                    <span><i class="fad fa-circle" style="color: #b9b1cc;"></i></span>
                                    <span><i class="fad fa-circle" style="color: #dddddd;"></i></span> -->
                                </div>
                            </div>
                            <div class="fiter-category">
                                <button type="button" class="filter__btns by__price-btn" id="btnSize">
                                    по размеру
                                </button>
                                <div class="fiter-caterory-content">
                                    <ul>
                                        <li><a href="#">46</a></li>
                                        <li><a href="#">48</a></li>
                                        <li><a href="#">50</a></li>
                                        <li><a href="#">52</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6"></div>
                </div>
                <div class="row" id="catalog__body-container_items">

                </div>
                <div class="row" id="catalog__body-container_pagination">
                    
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once("footer.php") ?>

