<?php
    require_once("header.php");
?>
<div class="item__card">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="title">your shopping cart qualifies for free shipping within the uk</h1>
            </div>
        </div>
        <div class="row mt-5 mb-5">
            <div class="col-lg-12">
                <div class="bread__crumbs">
                    <a href="#" class='crumb'>главная</a><span>карточка тура</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-1 px-0">
                <div class="slider slider-nav">
                    <div><img src="./images/product-image_01.png"></div>
                    <div><img src="./images/product-image_01.png"></div>
                    <div><img src="./images/product-image_01.png"></div>
                    <div><img src="./images/product-image_01.png"></div>
                    <div><img src="./images/product-image_01.png"></div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="slider  slider-for">
                    <div><img src="./images/product-image_01.png"></div>
                    <div><img src="./images/product-image_01.png"></div>
                    <div><img src="./images/product-image_01.png"></div>
                    <div><img src="./images/product-image_01.png"></div>
                    <div><img src="./images/product-image_01.png"></div>
                </div>
            </div>
            <div class="col-lg-5">
                
                <div class="item__information">
                    <h1 class="title">
                        пиджак в полоску sandsend,  бежевый
                    </h1>
                    <div class="item__status">
                        <div class="item__status__label--sale">sale</div>
                        
                    </div>
                    <div class="d-flex justify-content-between mt-4">
                        <div class="parametrs__wrapper">
                            <label for="parametr__size">размер</label>
                            <select id="parametr__size">
                            </select>
                        </div>
                        <div class="parametrs__wrapper">
                            <label for="parametr__color">цвет</label>
                            <select id="parametr__color">
                            </select>
                        </div>
                        <div class="parametrs__wrapper">
                            <label>материал</label>
                            <span>лён</span>
                        </div>
                        <div class="parametrs__wrapper">
                            <label>артикул</label>
                            <span>11077</span>
                        </div>
                    </div>
                    <div class="item__description">
                        <p>Long line and lightweight linen jacket with a single button fastening and two pockets. A UK size 6 - 8 can expect a slouchy fit whilst a UK size 14 will wear more fitted.</p>
                        <p>One Size: Length: 73cm, Across Width (Bust): 51cm, Sleeve: 45cm.</p>
                        <p>100% Linen.</p>
                        <div>
                            <p class="item__price">13980 kzt</p>
                        </div>
                    </div>
                    <div class="d-flex justify-content-between mt-4">
                        <button class="basket-add" data-panel="main">в корзину</button>
                        <button class="favorites-add">в избранное</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 px-0">
                <div class="extra">
                    <ul class="nav nav-pills nav-justified mt-5 mb-5" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-similar-tab" data-toggle="pill" href="#pills-similar" role="tab" aria-controls="pills-similar" aria-selected="true">похожие товары</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-finished-tab" data-toggle="pill" href="#pills-finished" role="tab" aria-controls="pills-finished" aria-selected="false">готовый образ</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-similar" role="tabpanel" aria-labelledby="pills-similar-tab">
                            <div class='items__carousel slider'>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-finished" role="tabpanel" aria-labelledby="pills-finished-tab">
                            <div class='items__carousel slider'>
                            </div>
                        </div>
                    </div>
                </div>      
            </div>
        </div>
    </div>
    <div class="cd-basket cd-basket--from-right">
		<header class="cd-basket__header">
			<h1>каталог</h1>
			<a href="#0" class="cd-basket__close js-cd-close"></a>
		</header>

		<div class="cd-basket__container">
			<div class="cd-basket__content">
				<div class="cd-basket__item">
                    <img src="./images/basket-image_01.png">
                    <p class="cd-basket__item-description">пиджак в полоску sandsend 6/xxs</p>
                    <div class="cd-basket__item-countNprice">
                        <form>
                            <div class="value-button" id="decrease" value="Decrease Value">-</div>
                            <input type="number" id="count" value="1"/>
                            <div class="value-button" id="increase" value="Increase Value">+</div>
                        </form>
                        <p class="price">66 900 kzt</p>
                    </div>
                </div>
                <h1 class="total__price">
                    итого: <span>66 900 kzt</span>
                </h1>
                <button class="pay">оплатить заказ</button>
			</div>
		</div>
	</div>
</div>
<?php
    require_once("footer.php");
?>