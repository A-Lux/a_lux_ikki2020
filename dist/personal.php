<?php
    require_once("header.php")
?>
    <div class="personal">
        <div class="container">
            <div class="row mt-5">
                <div class="col-lg-12">
                    <h1 class="title">личный кабинет</h1>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-lg-3">
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link active" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="true">Профиль</a>
                        <a class="nav-link" id="v-pills-order-tab" data-toggle="pill" href="#v-pills-order" role="tab" aria-controls="v-pills-order" aria-selected="false">Мои заказы</a>
                        <a class="nav-link" id="v-pills-promotional-tab" data-toggle="pill" href="#v-pills-promotional" role="tab" aria-controls="v-pills-promotional" aria-selected="false">Промокоды</a>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                            <form name="personal__form" class="row">
                                <div class="col-lg-6">
                                    <div class="mb-4">
                                        <h2 class="title">личные данные</h2>
                                    </div>
                                    <div class="form-group">
                                        <label for="fname">Имя</label>
                                        <input id="fname" class="form-control" name="fname" type="text" placeholder="Карина">
                                    </div>
                                    <div class="form-group">
                                        <label for="sname">Фамилия</label>
                                        <input id="sname" class="form-control" name="sname" type="text" placeholder="Исмаилова">
                                    </div>
                                    <div class="form-group">
                                        <label>Дата рождения</label>
                                        <div class="select-wrapper birthdate">
                                            <select name="birthday" class="birthday">
                                            <option value="4">4</option>
                                            </select>
                                            <select name="birthmonth" class="birthmonth">
                                            <option value="4">4</option>
                                            </select>
                                            <select name="birthyear" class="birthyear">
                                            <option value="4">4</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="phone">Телефон</label>
                                        <input id="phone" class="form-control" name="phone" type="text" placeholder="+7 (777) 366 66 91">
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Пароль</label>
                                        <input id="password" class="form-control" name="password" type="password" placeholder="Сменить пароль">
                                    </div>
                                    <div class="form-group">
                                        <label>Размер одежды</label>
                                        <select name="csize" class="csize">
                                        <option value="4">4</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Размер обуви</label>
                                        <select name="ssize" class="ssize">
                                        <option value="4">4</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class='d-flex justify-content-between mb-4'>
                                        <h2 class="title">адрес доставки</h2>
                                        <button type="button" class="edit">
                                            <span>Редактировать</span>
                                        </button>
                                    </div>
                                    <div class="form-group">
                                        <label for="city">Город</label>
                                        <input id="city" class="form-control" name="city" type="text" placeholder="Алматы">
                                    </div>
                                    <div class="form-group">
                                        <label for="street">Улица</label>
                                        <input id="street" class="form-control" name="street" type="text" placeholder="Торайгырова">
                                    </div>
                                    <div class="form-group justify-content-end">
                                        <div class="select-wrapper address">
                                            <div class="address-wrapper">
                                                <label for="door">Дом</label>
                                                <input id="door" type="text" name="door" class="form-control" placeholder="4">
                                            </div>
                                            <div class="address-wrapper">
                                                <label for="room">Квартира/офис</label>
                                                <input id="room" type="text" name="room" class="form-control" placeholder="4">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group justify-content-end">
                                        <div class="select-wrapper address">
                                            <div class="address-wrapper">
                                                <label for="porch">Подъезд</label>
                                                <input id="porch" type="text" name="porch" class="form-control" placeholder="4">
                                            </div>
                                            <div class="address-wrapper">
                                                <label for="floor">Этаж</label>
                                                <input id="floor" type="text" name="floor" class="form-control" placeholder="4">
                                            </div>
                                        </div>
                                    </div>
                                    <div id="more-container">

                                    </div>
                                    <div class="form-group justify-content-end">
                                        <div class="add__container">
                                            <button type="button" name="add-more" class="add-more">Добавить адрес</button>
                                        </div>
                                    </div>
                                    <div class="form-group justify-content-end mt-5">
                                        <button type="submit" class="save">сохранить</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="v-pills-order" role="tabpanel" aria-labelledby="v-pills-order-tab">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="mb-4">
                                        <h2 class="title">списко заказов</h2>
                                    </div>
                                    <div class="row order__header">
                                        <div class="col-lg-2 col-3">
                                            <p class="order__number-title">
                                                № заказа
                                            </p>
                                        </div>
                                        <div class="col-lg-6 col-3">
                                            <a href="#" class="order__by-date">Дата</a>
                                        </div>
                                        <div class="col-lg-2 col-3">
                                            <p class="order__count-title">Количество</p>
                                        </div>
                                        <div class="col-lg-2 col-3">
                                            <a href="#" class="order__by-status">Статус</a>
                                        </div>
                                    </div>
                                    <div class="row order__info">
                                        <div class="col-lg-2 col-3">
                                            <p class="order__number">125854</p>
                                        </div>
                                        <div class="col-lg-6 col-3">
                                            <p class="order__date">12.10.2019</p>
                                        </div>
                                        <div class="col-lg-2  col-2">
                                            <p class="order__count">12 шт</p>
                                        </div>
                                        <div class="col-lg-2 col-4">
                                            <p class="order__status">в обработке</p>
                                        </div>
                                    </div>
                                    <div class="row order__info">
                                        <div class="col-lg-2 col-3">
                                            <p class="order__number">125854</p>
                                        </div>
                                        <div class="col-lg-6 col-3">
                                            <p class="order__date">12.10.2019</p>
                                        </div>
                                        <div class="col-lg-2 col-2">
                                            <p class="order__count">12 шт</p>
                                        </div>
                                        <div class="col-lg-2 col-4">
                                            <p class="order__status">в обработке</p>
                                        </div>
                                    </div>
                                    <div class="row order__info">
                                        <div class="col-lg-2 col-3">
                                            <p class="order__number">125854</p>
                                        </div>
                                        <div class="col-lg-6 col-3">
                                            <p class="order__date">12.10.2019</p>
                                        </div>
                                        <div class="col-lg-2 col-2">
                                            <p class="order__count">12 шт</p>
                                        </div>
                                        <div class="col-lg-2 col-4">
                                            <p class="order__status">в обработке</p>
                                        </div>
                                    </div>
                                    <div class="row order__info">
                                        <div class="col-lg-2 col-3">
                                            <p class="order__number">125854</p>
                                        </div>
                                        <div class="col-lg-6 col-3">
                                            <p class="order__date">12.10.2019</p>
                                        </div>
                                        <div class="col-lg-2 col-2">
                                            <p class="order__count">12 шт</p>
                                        </div>
                                        <div class="col-lg-2 col-4">
                                            <p class="order__status">в обработке</p>
                                        </div>
                                    </div>
                                    <div class="row order__info">
                                        <div class="col-lg-2 col-3">
                                            <p class="order__number">125854</p>
                                        </div>
                                        <div class="col-lg-6 col-3">
                                            <p class="order__date">12.10.2019</p>
                                        </div>
                                        <div class="col-lg-2 col-2">
                                            <p class="order__count">12 шт</p>
                                        </div>
                                        <div class="col-lg-2 col-4">
                                            <p class="order__status">в обработке</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-promotional" role="tabpanel" aria-labelledby="v-pills-promotional-tab">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h2 class="title">бонусы</h2>
                                    <div class="row bonus__header">
                                        <div class="col-lg-2 col-3">
                                            <p class="bonus__date-title">
                                                Дата
                                            </p>
                                        </div>
                                        <div class="col-lg-6 col-3">
                                            <p class="bonus__name-title">Наименование промокода</p>
                                        </div>
                                        <div class="col-lg-2 col-3">
                                            <p class="bonus__status-title">Статус</p>
                                        </div>
                                        <div class="col-lg-2 col-3">
                                        </div>
                                    </div>
                                    <div class="row bonus__info active">
                                        <div class="col-lg-2 col-3">
                                            <p class="bonus__date">12.10.2019</p>
                                        </div>
                                        <div class="col-lg-6 col-3">
                                            <p class="bonus__name">VESNA2020</p>
                                        </div>
                                        <div class="col-lg-2 col-2">
                                            <p class="bonus__status">доступен</p>
                                        </div>
                                        <div class="col-lg-2 col-4">
                                            <a href="#" class="bonus__use">применить</a>
                                        </div>
                                    </div>
                                    <div class="row bonus__info active">
                                        <div class="col-lg-2 col-3">
                                            <p class="bonus__date">12.10.2019</p>
                                        </div>
                                        <div class="col-lg-6 col-3">
                                            <p class="bonus__name">VESNA2020</p>
                                        </div>
                                        <div class="col-lg-2 col-2">
                                            <p class="bonus__status">доступен</p>
                                        </div>
                                        <div class="col-lg-2 col-4">
                                            <a href="#" class="bonus__use">применить</a>
                                        </div>
                                    </div>
                                    <div class="row bonus__info">
                                        <div class="col-lg-2 col-3">
                                            <p class="bonus__date">12.10.2019</p>
                                        </div>
                                        <div class="col-lg-6 col-3">
                                            <p class="bonus__name">VESNA2020</p>
                                        </div>
                                        <div class="col-lg-2 col-2">
                                            <p class="bonus__status">использован</p>
                                        </div>
                                        <div class="col-lg-2 col-4">
                                            <a href="#" class="bonus__use">применить</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
    require_once("footer.php")
?>