<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>IKKI</title>
    <link rel="stylesheet" href="./css/main.css">
</head>

<body>
    <div class="header">
        <div class="container mt-3 mb-3">
            <div class="row">
                <div class="col-lg-2">
                    <a href="#" class="logo">
                        <img src="./images/ikki.png" alt="logo">
                    </a>
                </div>
                <div class="col-lg-3">
                    <nav class="navbar navbar-expand-lg navbar-light" id="main_navbar">
                        <ul class="navbar-nav">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    новинки
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <li class="nav-item dropdown">
                                        <a class="dropdown-item dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            одежда
                                        </a>
                                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
                                            <li><a class="dropdown-item" href="#">Пиджаки и костюмы</a></li>
                                            <li><a class="dropdown-item" href="#">Платья и сарафаны</a></li>
                                            <li><a class="dropdown-item" href="#">Спортивные костюмы</a></li>
                                            <li><a class="dropdown-item" href="#">Толстовки и олимпийки</a></li>
                                            <li><a class="dropdown-item" href="#">Топы и майки</a></li>
                                            <li><a class="dropdown-item" href="#">Туники</a></li>
                                        </ul>
                                    </li>
                                    <li><a class="dropdown-item" href="#">обувь</a></li>
                                    <li><a class="dropdown-item" href="#">аксессуары</a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="catalog.php" id="navbarDropdown" >
                                    каталог
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <li class="nav-item dropdown">
                                        <a class="dropdown-item dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            одежда
                                        </a>
                                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
                                            <li><a class="dropdown-item" href="#">Пиджаки и костюмы</a></li>
                                            <li><a class="dropdown-item" href="#">Платья и сарафаны</a></li>
                                            <li><a class="dropdown-item" href="#">Спортивные костюмы</a></li>
                                            <li><a class="dropdown-item" href="#">Толстовки и олимпийки</a></li>
                                            <li><a class="dropdown-item" href="#">Топы и майки</a></li>
                                            <li><a class="dropdown-item" href="#">Туники</a></li>
                                        </ul>
                                    </li>
                                    <li><a class="dropdown-item" href="#">обувь</a></li>
                                    <li><a class="dropdown-item" href="#">аксессуары</a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    sale
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <li class="nav-item dropdown">
                                        <a class="dropdown-item dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            одежда
                                        </a>
                                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
                                            <li><a class="dropdown-item" href="#">Пиджаки и костюмы</a></li>
                                            <li><a class="dropdown-item" href="#">Платья и сарафаны</a></li>
                                            <li><a class="dropdown-item" href="#">Спортивные костюмы</a></li>
                                            <li><a class="dropdown-item" href="#">Толстовки и олимпийки</a></li>
                                            <li><a class="dropdown-item" href="#">Топы и майки</a></li>
                                            <li><a class="dropdown-item" href="#">Туники</a></li>
                                        </ul>
                                    </li>
                                    <li><a class="dropdown-item" href="#">обувь</a></li>
                                    <li><a class="dropdown-item" href="#">аксессуары</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xl-7">
                    <div class="user__cabinet">
                        <div class="user__cabinet-item">
                            <a href="#">
                                <p>Личный кабинет</p>
                                <i class="fal fa-user"></i>
                            </a>
                        </div>
                        <div class="user__cabinet-item">
                            <a href="#">
                                <span>Корзина</span>
                                <i class="fal fa-shopping-bag"></i>
                            </a>
                        </div>
                        <div class="user__cabinet-item">
                            <a href="#">
                                <span>Избранное</span>
                                <i class="fal fa-heart"></i>
                            </a>
                        </div>
                        <div class="user__cabinet-search">
                            <input type="text" placeholder="Поиск" class="input">
                            <a href="#" class="search-hover">
                                <i class="fal fa-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>