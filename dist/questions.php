<?php require_once("header.php"); ?>

<div class="questions">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="bread__crumbs">
                    <a href="#" class='crumb'>главная</a><span>вопросы и Ответы</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="questions__title--wrap">
                    <h3 class="questions__title">Вопросы и Ответы</h3>

                    <a href="#" class="questions__ask" data-toggle="modal" data-target="#questions-modal">
                        Задать вопрос
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="accordion--wrapper">
                    <div class="accordion" id="accordionExample">
                        <div class="card">
                            <div class="card-header px-0" id="headingOne">
                                <span>01</span>
                                <h2 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Как войти в личный кабинет?
                                    </button>
                                </h2>
                            </div>

                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body">
                                    Возврат и обмен возможен в течение 14 календарных дней со дня получения посылки, для
                                    этого вам необходимо обратиться на наш электронный ящик info@fason-m.com.

                                    И заполнить Google форму для возврата средств:
                                    https://docs.google.com/forms/d/1_VrVjoZI2nsX1zqcWKETfYt42yMMFxn7fN-TOiQ468Q/edit


                                    Укажите номер Вашего заказа, модель и причину возврата.
                                    Возврат возможен, если соблюдены следующие условия: отсутствуют следы использования
                                    товара, сохранен его товарный вид (бирка, упаковка).
                                    Транспортные расходы при возврате и обмене оплачиваются покупателем, кроме случаев
                                    производственного брака и пересорта.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header px-0" id="headingTwo">
                                <span>02</span>
                                <h2 class="mb-0">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Как подтвердить оплату в личном кабинете?
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body">
                                    Возврат и обмен возможен в течение 14 календарных дней со дня получения посылки, для
                                    этого вам необходимо обратиться на наш электронный ящик info@fason-m.com.

                                    И заполнить Google форму для возврата средств:
                                    https://docs.google.com/forms/d/1_VrVjoZI2nsX1zqcWKETfYt42yMMFxn7fN-TOiQ468Q/edit


                                    Укажите номер Вашего заказа, модель и причину возврата.
                                    Возврат возможен, если соблюдены следующие условия: отсутствуют следы использования
                                    товара, сохранен его товарный вид (бирка, упаковка).
                                    Транспортные расходы при возврате и обмене оплачиваются покупателем, кроме случаев
                                    производственного брака и пересорта.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header px-0" id="headingThree">
                                <span>03</span>
                                <h2 class="mb-0">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Возврат и обмен
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card-body">
                                    Возврат и обмен возможен в течение 14 календарных дней со дня получения посылки, для
                                    этого вам необходимо обратиться на наш электронный ящик info@fason-m.com.

                                    И заполнить Google форму для возврата средств:
                                    https://docs.google.com/forms/d/1_VrVjoZI2nsX1zqcWKETfYt42yMMFxn7fN-TOiQ468Q/edit


                                    Укажите номер Вашего заказа, модель и причину возврата.
                                    Возврат возможен, если соблюдены следующие условия: отсутствуют следы использования
                                    товара, сохранен его товарный вид (бирка, упаковка).
                                    Транспортные расходы при возврате и обмене оплачиваются покупателем, кроме случаев
                                    производственного брака и пересорта.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header px-0" id="headingFour">
                                <span>04</span>
                                <h2 class="mb-0">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        Что делать, если пароль утерян?
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                                <div class="card-body">
                                    Возврат и обмен возможен в течение 14 календарных дней со дня получения посылки, для
                                    этого вам необходимо обратиться на наш электронный ящик info@fason-m.com.

                                    И заполнить Google форму для возврата средств:
                                    https://docs.google.com/forms/d/1_VrVjoZI2nsX1zqcWKETfYt42yMMFxn7fN-TOiQ468Q/edit


                                    Укажите номер Вашего заказа, модель и причину возврата.
                                    Возврат возможен, если соблюдены следующие условия: отсутствуют следы использования
                                    товара, сохранен его товарный вид (бирка, упаковка).
                                    Транспортные расходы при возврате и обмене оплачиваются покупателем, кроме случаев
                                    производственного брака и пересорта.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header px-0" id="headingFive">
                                <span>05</span>
                                <h2 class="mb-0">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                        Как скачать каталоги?
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                                <div class="card-body">
                                    Возврат и обмен возможен в течение 14 календарных дней со дня получения посылки, для
                                    этого вам необходимо обратиться на наш электронный ящик info@fason-m.com.

                                    И заполнить Google форму для возврата средств:
                                    https://docs.google.com/forms/d/1_VrVjoZI2nsX1zqcWKETfYt42yMMFxn7fN-TOiQ468Q/edit


                                    Укажите номер Вашего заказа, модель и причину возврата.
                                    Возврат возможен, если соблюдены следующие условия: отсутствуют следы использования
                                    товара, сохранен его товарный вид (бирка, упаковка).
                                    Транспортные расходы при возврате и обмене оплачиваются покупателем, кроме случаев
                                    производственного брака и пересорта.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header px-0" id="headingSix">
                                <span>06</span>
                                <h2 class="mb-0">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                        Как скачать каталоги?
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
                                <div class="card-body">
                                    Возврат и обмен возможен в течение 14 календарных дней со дня получения посылки, для
                                    этого вам необходимо обратиться на наш электронный ящик info@fason-m.com.

                                    И заполнить Google форму для возврата средств:
                                    https://docs.google.com/forms/d/1_VrVjoZI2nsX1zqcWKETfYt42yMMFxn7fN-TOiQ468Q/edit


                                    Укажите номер Вашего заказа, модель и причину возврата.
                                    Возврат возможен, если соблюдены следующие условия: отсутствуют следы использования
                                    товара, сохранен его товарный вид (бирка, упаковка).
                                    Транспортные расходы при возврате и обмене оплачиваются покупателем, кроме случаев
                                    производственного брака и пересорта.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header px-0" id="headingSeven">
                                <span>07</span>
                                <h2 class="mb-0">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                        Как скачать каталоги?
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionExample">
                                <div class="card-body">
                                    Возврат и обмен возможен в течение 14 календарных дней со дня получения посылки, для
                                    этого вам необходимо обратиться на наш электронный ящик info@fason-m.com.

                                    И заполнить Google форму для возврата средств:
                                    https://docs.google.com/forms/d/1_VrVjoZI2nsX1zqcWKETfYt42yMMFxn7fN-TOiQ468Q/edit


                                    Укажите номер Вашего заказа, модель и причину возврата.
                                    Возврат возможен, если соблюдены следующие условия: отсутствуют следы использования
                                    товара, сохранен его товарный вид (бирка, упаковка).
                                    Транспортные расходы при возврате и обмене оплачиваются покупателем, кроме случаев
                                    производственного брака и пересорта.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header px-0" id="headingEight">
                                <span>08</span>
                                <h2 class="mb-0">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                        Как скачать каталоги?
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordionExample">
                                <div class="card-body">
                                    Возврат и обмен возможен в течение 14 календарных дней со дня получения посылки, для
                                    этого вам необходимо обратиться на наш электронный ящик info@fason-m.com.

                                    И заполнить Google форму для возврата средств:
                                    https://docs.google.com/forms/d/1_VrVjoZI2nsX1zqcWKETfYt42yMMFxn7fN-TOiQ468Q/edit


                                    Укажите номер Вашего заказа, модель и причину возврата.
                                    Возврат возможен, если соблюдены следующие условия: отсутствуют следы использования
                                    товара, сохранен его товарный вид (бирка, упаковка).
                                    Транспортные расходы при возврате и обмене оплачиваются покупателем, кроме случаев
                                    производственного брака и пересорта.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">

            </div>
            <div class="col-lg-6">
                <h1 class="instagram__title">наш instagram</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="instagram__carousel">
                    <div class="owl-carousel owl-theme">
                        <div class="item">
                            <a href="#">
                                <img src="./images/instagram-image_01.png">
                            </a>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="./images/instagram-image_02.png">
                            </a>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="./images/instagram-image_03.png">
                            </a>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="./images/instagram-image_04.png">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade modal-quest" id="questions-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Задайте вопрос</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="">
                    <label for="">Имя</label>
                    <input type="text" placeholder="Ваше имя">
                    <br>
                    <label for="">Номер</label>
                    <input type="text" placeholder="Ваш номер" id="phone"> 
                    <br>
                    <label for="">Вопрос</label>
                    <textarea name="" id=""  placeholder="Вопрос"></textarea>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="button-ikki">Отравить</button>
            </div>
        </div>
    </div>
</div>

<?php require_once("footer.php"); ?>