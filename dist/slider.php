<div class="component-slider">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="slider__h2">
                    НАШ INSTAGRAM
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="instagram__carousel">
                    <div class="owl-carousel owl-theme">
                        <div class="item">
                            <a href="#">
                                <img src="./images/instagram-image_01.png">
                            </a>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="./images/instagram-image_02.png">
                            </a>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="./images/instagram-image_03.png">
                            </a>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="./images/instagram-image_04.png">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    require_once("footer.php")
?>