<?php
    require_once("header.php");
?>
<div class="welcome">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="main__banner">
                    <div class="owl-carousel owl-theme">
                        <div class="item">
                            <img src="./images/slide-image_01.png">
                        </div>
                        <div class="item">
                            <img src="./images/slide-image_01.png">
                        </div>
                        <div class="item">
                            <img src="./images/slide-image_01.png">
                        </div>
                    </div>
                    <div class="slider-counter"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <h1 class="catalog__title">каталог</h1>
            </div>
            <div class="col-lg-6"></div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="d-flex flex-column justify-content-between h-100">
                    <div class="catalog__item" data-title='аксессуары'>
                        <img src="./images/catalog-image_02.png">
                    </div>
                    <div class="catalog__item" data-title='обувь'>
                        <img src="./images/catalog-image_03.png">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="catalog__item" data-title='одежда'>
                    <img src="./images/catalog-image_01.png">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">

            </div>
            <div class="col-lg-6">
                <h1 class="instagram__title">наш instagram</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="instagram__carousel">
                    <div class="owl-carousel owl-theme">
                        <div class="item">
                            <a href="#">
                                <img src="./images/instagram-image_01.png">
                            </a>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="./images/instagram-image_02.png">
                            </a>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="./images/instagram-image_03.png">
                            </a>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="./images/instagram-image_04.png">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    require_once("footer.php")
?>
