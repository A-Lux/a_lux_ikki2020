import 'bootstrap';
window.$ = window.jQuery = require('jquery');
const owl_carousel = require('owl.carousel');
const offcanvas = require('hc-offcanvas-nav');
const Swal = require('sweetalert2');
const slick = require('slick-carousel');
const select2 = require('select2');
const axios = require('axios'); 
const mask = require('jquery-mask-plugin');
// import 'jquery-mask-plugin';
(function ($) {
    $.fn.bootnavbar = function () {
        $(this).find('.dropdown').hover(function () {
            $(this).addClass('show');
            $(this).find('.dropdown-menu').first().addClass('show').addClass('animated fadeIn').one('animationend oAnimationEnd mozAnimationEnd webkitAnimationEnd', function () {
                $(this).removeClass('animated fadeIn');
            });
        }, function () {
            $(this).removeClass('show');
            $(this).find('.dropdown-menu').first().removeClass('show');
        });
    };
})(jQuery);
$("input[name='phone']").mask("+7 (999) 999 99 99");
$(function () {
   
    $('#main_navbar').bootnavbar();
})
if ($('div').is('.welcome')) {
    $('.main__banner .owl-carousel').on('initialized.owl.carousel changed.owl.carousel', function (e) {
        if (!e.namespace) {
            return;
        }
        let carousel = e.relatedTarget;
        $('.main__banner .slider-counter').html(`<p class="slide-current">0${carousel.relative(carousel.current()) + 1}</p>` + '<div class="slide-progress"></div>' + `<p class="slide-length">0${carousel.items().length}</p>`);
    }).owlCarousel({
        items: 1,
        loop: true,
        autoplay: true,
        margin: 0,
        nav: true,
        navText: ["<img src='./images/slider-arrow-left.png'>", "<img src='./images/slider-arrow-right.png'>"],
        dots: false,
        onInitialized: startProgressBar,
        onTranslate: resetProgressBar,
        onTranslated: startProgressBar
    });
    function startProgressBar() {
        // apply keyframe animation
        $(".slide-progress").css({
            width: "78px",
            transition: "width 5000ms"
        });
    }

    function resetProgressBar() {
        $(".slide-progress").css({
            width: 0,
            transition: "width 0s"
        });
    }
    $('.instagram__carousel .owl-carousel').owlCarousel({
        items: 4,
        loop: true,
        autoplay: true,
        margin: 30,
        nav: false,
        dots: false,
    });
}

else if ($('div').is('.about,.questions,.component-slider')) {
    $('.instagram__carousel .owl-carousel').owlCarousel({
        items: 4,
        loop: true,
        autoplay: true,
        margin: 30,
        nav: false,
        dots: false,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 3,
            },
            1000: {
                items: 4,
                nav: true,

            }
        }
    });

    if ($("div").is(".questions")) {
        let collapseList = document.querySelectorAll('.btn')
        let collapse = document.querySelectorAll('.btn').forEach((collapse) => {
            collapse.addEventListener('click', () => {
                for (let i = 0; i < collapseList.length; i++) {
                    collapseList[i].parentNode.parentNode.classList.remove("highlightNumber");
                    collapseList[i].parentNode.parentNode.classList.remove("collapse-sign");
                }
                if (collapse.parentNode.parentNode.parentNode.children[1].classList.contains("show") === false) {
                    console.log(collapse.parentNode.parentNode.parentNode.children[1].classList.contains("show"))
                    collapse.parentNode.parentNode.classList.add("collapse-sign");
                    collapse.parentNode.parentNode.classList.add("highlightNumber");
                } else if (collapse.parentNode.parentNode.parentNode.children[1].classList.contains("show") === true) {
                    console.log(collapse.parentNode.parentNode.parentNode.children[1].classList.contains("show"))
                    collapse.parentNode.parentNode.classList.remove("collapse-sign");
                    collapse.parentNode.parentNode.classList.remove("highlightNumber");
                }
            });
        });
    }

}

else if ($('div').is('.item__card')) {
    $('body').children().wrapAll('<div></div>');
    let body = document.getElementsByTagName('body')[0];
    body.style.overflowY = 'hidden'
    body.firstElementChild.classList.add('item__card-container');
    let similar__container = document.getElementById('pills-similar').firstElementChild;
    let finished__container = document.getElementById('pills-finished').firstElementChild;
    let items = [];
    let colorArr;
    let icons = [];
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        asNavFor: '.slider-nav',
        draggable: false,
        centerMode: false,
    });
    $('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        arrows: false,
        autoplay: false,
        autoplaySpeed: 3000,
        centerMode: false,
        focusOnSelect: true,
        vertical: true,
        draggable: false,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    vertical: false,
                    arrows: false,
                }
            }
        ]
    });
    function increaseValue() {
        var value = parseInt(document.getElementById('count').value, 10);
        value = isNaN(value) ? 0 : value;
        value++;
        document.getElementById('count').value = value;
    }
      
    function decreaseValue() {
        var value = parseInt(document.getElementById('count').value, 10);
        value = isNaN(value) ? 0 : value;
        value < 1 ? value = 1 : '';
        value--;
        document.getElementById('count').value = value;
    }
    document.getElementById('increase').addEventListener('click', function() {
        increaseValue();
    })
    document.getElementById('decrease').addEventListener('click', function() {
        decreaseValue();
    })
    function colorSlider() {
        document.querySelectorAll('.item__colors .fa-circle').forEach(circle => circle.addEventListener('click', function () {
            $(`img[data-id=${this.getAttribute('data-id')}]`)
                .fadeOut(400, function () {
                    $(`img[data-id=${this.getAttribute('data-id')}]`).attr('src', circle.getAttribute('data-src'));
                })
                .fadeIn(400);
        }));
    }

    $('#parametr__size').select2({
        minimumResultsForSearch: Infinity,
        width: '100%',
        ajax: {
            url: './items/card.json',
            dataType: 'json',
            processResults: function(data) {
                for (const d of data.results) {
                    if (d.hasOwnProperty('size')) {
                        d.text = d.size;
                    }
                }
                return {
                    results: data.results
                }
            }
        }
    });
    $('#parametr__color').select2({
        disabled: false,
        minimumResultsForSearch: Infinity,
        width: '100%',
        escapeMarkup: function (markup) {
            return markup;
        },
        ajax: {
            url: './items/card.json',
            dataType: 'json',
            processResults: function(data) {
                for (const d of data.results) {
                    d.colors.forEach(color => {
                        d.text =`<i style="font-size: 20px; color: ${color}" class="fad fa-circle"></i>`
                    })
                }
                
                return {
                    results: data.results
                }
            }
        }
    })
    $('#parametr__size').on('select2:select', function (e) {
        
    });
    // axios.get(`./items/colors.json`)
    //     .then(colors => {
    //         colorArr = colors.data.results;
    //         colorArr.forEach(color => {
    //             icons.push({
    //                 id: color.id,
    //                 text: `<i title=${color.text} style="font-size: 20px; color: ${color.code}" class="fad fa-circle"></i>`,
    //                 selected: color.selected ? true : false,
    //                 disabled: color.disabled ? true : false
    //             });
    //         });
    //         $('#parametr__color').select2({
    //             minimumResultsForSearch: Infinity,
    //             data: icons,
    //             escapeMarkup: function (markup) {
    //                 return markup;
    //             },
    //             width: '100%',
    //         });
    //     });
    

    axios.get('./items/catalog.json')
        .then(catalog => {

            items = catalog.data.items;

            items.forEach((item, i) => {
                if (i < 7) {
                    similar__container.appendChild(renderItemCarousel(item));
                }
                else {
                    return;
                }
            });
            itemsCarouselMaker();
            colorSlider();
        });
    document.querySelector('a[href="#pills-finished"]').addEventListener('click', function () {
        finished__container.innerHTML = '';
        similar__container.innerHTML = '';
        items.forEach((item, i) => {
            if (i < 7) {
                finished__container.appendChild(renderItemCarousel(item));
            }
            else {
                return;
            }
        });
        setTimeout(() => {
            itemsCarouselMaker()
        }, 200);
        colorSlider();
    });
    document.querySelector('a[href="#pills-similar"]').addEventListener('click', function () {
        similar__container.innerHTML = '';
        finished__container.innerHTML = '';
        items.forEach((item, i) => {
            if (i < 7) {
                similar__container.appendChild(renderItemCarousel(item));
            }
            else {
                return;
            }
        });
        setTimeout(() => {
            itemsCarouselMaker()
        }, 200);
        colorSlider();
    })
    function renderItemCarousel(item) {
        let wrapper = document.createElement('div');
        let item__image = document.createElement('div');
        item__image.classList.add('item__image');
        let img = document.createElement('img');
        img.setAttribute('data-id', item.id);
        img.src = Object.values(item.images)[0];
        let favorites__add = document.createElement('a');
        favorites__add.classList.add('favorites-add');
        favorites__add.href = '#';
        let fa__heart = document.createElement('i');
        fa__heart.classList.add('fad', 'fa-heart');
        favorites__add.appendChild(fa__heart);
        item__image.appendChild(img);
        item__image.appendChild(favorites__add);
        let item__colors = document.createElement('ul');
        item__colors.classList.add('item__colors');
        for (const [key, value] of Object.entries(item.images)) {
            let li = document.createElement('li');
            let fa__circle = document.createElement('i');
            fa__circle.classList.add('fad', 'fa-circle');
            fa__circle.style.color = key;
            fa__circle.setAttribute('data-id', item.id);
            fa__circle.setAttribute('data-src', value)
            li.appendChild(fa__circle);
            item__colors.appendChild(li);
        }
        let item__status = document.createElement('div');
        item__status.classList.add('item__status');
        if (item.is_new) {
            let item__status__label_new = document.createElement('div');
            item__status__label_new.classList.add('item__status__label--new');
            item__status__label_new.innerText = 'new';
            item__status.appendChild(item__status__label_new);
        }
        if (item.is_sale) {
            let item__status__label_sale = document.createElement('div');
            item__status__label_sale.classList.add('item__status__label--sale');
            item__status__label_sale.innerText = 'sale';
            item__status.appendChild(item__status__label_sale);
        }
        let item__title = document.createElement('p');
        item__title.classList.add('item__title');
        item__title.innerText = item.title;
        let item__prices = document.createElement('div');
        item__prices.classList.add('item__prices');
        let item__price = document.createElement('p');
        item__price.classList.add('item__price');
        item__price.innerText = item.price;
        item__prices.appendChild(item__price);
        if (item.old_price) {
            let item__old_price = document.createElement('p');
            item__old_price.classList.add('item__old-price');
            item__old_price.innerText = item.old_price;
            item__prices.appendChild(item__old_price);

        }
        wrapper.appendChild(item__image);
        wrapper.appendChild(item__colors);
        wrapper.appendChild(item__status);
        wrapper.appendChild(item__title);
        wrapper.appendChild(item__prices);
        return wrapper;
    }
    function itemsCarouselMaker() {
        if (document.querySelector('.tab-pane.active .items__carousel.slider.slick-initialized')) {
            $(document.querySelector('.tab-pane.active').firstElementChild).slick('refresh');
        }
        else {
            $(document.querySelector('.tab-pane.active').firstElementChild).slick({
                slidesToShow: 5,
                slidesToScroll: 1,
                infinite: true,
                autoplay: false,
                autoplaySpeed: 4000,
                dots: false,
                arrows: false
            });
        }
    }
    (function(){
        let panelTriggers = document.getElementsByClassName('basket-add');
        if( panelTriggers.length > 0 ) {
            for(let i = 0; i < panelTriggers.length; i++) {
                (function(i){
                    let x = null;
                    let panel = document.getElementsByClassName('cd-basket')[0];
                    let card__cotnainer = document.getElementsByClassName('item__card-container')[0];
                    panelTriggers[i].addEventListener('click', function(event){
                        event.preventDefault();
                        addClass(panel, 'cd-basket--is-visible');
                        addClass(card__cotnainer, 'slide__right');
                    });
                    panel.addEventListener('click', function (event) {
                        if (hasClass(event.target, 'js-cd-close') || hasClass(event.target, 'cd-basket')) {
                            event.preventDefault();
                            removeClass(panel, 'cd-basket--is-visible');
                            removeClass(card__cotnainer, 'slide__right');
                        }
                    });
                    document.addEventListener('mousemove', onMouseUpdate, false);
                    function onMouseUpdate(e) {
                        x = e.pageX;
                        if (x >= window.innerWidth - 1 && !hasClass(panel, 'js-cd-close') || !hasClass(panel, 'cd-basket')) {
                            addClass(panel, 'cd-basket--is-visible');
                            addClass(card__cotnainer, 'slide__right');    
                        }
                    }
                })(i);
            }
        }
        $(document).mouseup(function (e) {
            let panel = $('.cd-basket');
            let card__cotnainer = $('.item__card-container');
            if (!panel.is(e.target) && panel.has(e.target).length === 0) {
                panel.removeClass('cd-basket--is-visible');
                card__cotnainer.removeClass('slide__right');
            }
        });
        function hasClass(el, className) {
            if (el.classList) return el.classList.contains(className);
            else return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
        }
        function addClass(el, className) {
            if (el.classList) el.classList.add(className);
            else if (!hasClass(el, className)) el.className += " " + className;
        }
        function removeClass(el, className) {
            if (el.classList) el.classList.remove(className);
            else if (hasClass(el, className)) {
                let reg = new RegExp('(\\s|^)' + className + '(\\s|$)');
                el.className = el.className.replace(reg, ' ');
            }
        }
    })();
        
    
    
}

else if ($('div').is('.catalog')) {
    let btnSize = document.getElementById('btnSize');
    btnSize.addEventListener('click', function () {
        $(this).parent().toggleClass('active');
        $(this).next().slideToggle(300);
    });

    let colorOnce = true;
    $('.by__color-btn').on('click', function () {
        if (colorOnce) {
            axios.get('./items/colors.json')
                .then(color => {
                    let colorCode = [];
                    color = color.data.results;
                    color.forEach(colorCode => {
                        renderCatalogColors(colorCode);
                    })
                });
            colorOnce = false;
        }

        $(this).parent().toggleClass('active-color');
        $(this).next().slideToggle(100);

    });

    let filterColor = document.getElementsByClassName('fiter-color-content')[0];
    function renderCatalogColors(color) {
        let span = document.createElement('span');
        let icon = document.createElement('i');
        icon.classList.add('fad', 'fa-circle');
        icon.setAttribute('data-color', color.code);
        icon.style.color = color.code;
        filterColor.appendChild(span);
        span.appendChild(icon);

        icon.addEventListener('click', function () {
            span.addEventListener('click', function () {
                if (icon.getAttribute('data-color')) {
                    console.log('Data-color' + ' = ' + icon.getAttribute('data-color'));
                }
            })

        });
    }

    let perPage = 50;
    let items;
    let content = document.getElementById('catalog__body-container_items');
    let pagination = document.getElementById('catalog__body-container_pagination');
    content.innerHTML = '';

    function renderItem(item) {
        let wrapper = document.createElement('div');
        wrapper.classList.add('col-sm-12', 'col-md-6', 'col-lg-4', 'col-xl-3', 'catalog-item');
        let item__image = document.createElement('div');
        item__image.classList.add('item__image');
        let img = document.createElement('img');
        img.setAttribute('data-id', item.id);
        img.src = Object.values(item.images)[0];
        let favorites__add = document.createElement('a');
        favorites__add.classList.add('favorites-add');
        favorites__add.href = '#';
        let fa__heart = document.createElement('i');
        fa__heart.classList.add('fad', 'fa-heart');
        favorites__add.appendChild(fa__heart);
        item__image.appendChild(img);
        item__image.appendChild(favorites__add);
        let item__colors = document.createElement('ul');
        item__colors.classList.add('item__colors');
        for (const [key, value] of Object.entries(item.images)) {
            let li = document.createElement('li');
            let fa__circle = document.createElement('i');
            fa__circle.classList.add('fad', 'fa-circle');
            fa__circle.style.color = key;
            fa__circle.setAttribute('data-id', item.id);
            fa__circle.setAttribute('data-src', value);
            fa__circle.setAttribute('data-color', key)
            li.appendChild(fa__circle);
            item__colors.appendChild(li);
        }
        let item__status = document.createElement('div');
        item__status.classList.add('item__status');
        if (item.is_new) {
            let item__status__label_new = document.createElement('div');
            item__status__label_new.classList.add('item__status__label--new');
            item__status__label_new.innerText = 'new';
            item__status.appendChild(item__status__label_new);
        }
        if (item.is_sale) {
            let item__status__label_sale = document.createElement('div');
            item__status__label_sale.classList.add('item__status__label--sale');
            item__status__label_sale.innerText = 'sale';
            item__status.appendChild(item__status__label_sale);
        }
        let item__title = document.createElement('p');
        item__title.classList.add('item__title');
        item__title.innerText = item.title;
        let item__prices = document.createElement('div');
        item__prices.classList.add('item__prices');
        let item__price = document.createElement('p');
        item__price.classList.add('item__price');
        item__price.innerText = item.price;
        item__prices.appendChild(item__price);



        if (item.old_price) {
            let item__old_price = document.createElement('p');
            item__old_price.classList.add('item__old-price');
            item__old_price.innerText = item.old_price;
            item__prices.appendChild(item__old_price);
            // console.log(item__old_price);
            // console.log(item__price)
        }
        wrapper.appendChild(item__image);
        wrapper.appendChild(item__colors);
        wrapper.appendChild(item__status);
        wrapper.appendChild(item__title);
        wrapper.appendChild(item__prices);
        return wrapper;
    }



    $(document).ready(function () {
        function priceFilter() {
            let catalogItem = document.getElementsByClassName('catalog-item');
            let from = document.getElementById('from');
            let before = document.getElementById('before');
            let searchBtn = document.getElementById('searchBtn');
            let itemPrices = document.getElementsByClassName('item__price');
            let itemPricesOld = document.getElementsByClassName('item__old-price');
            $('#priceBtn').on('click', function () {
                $(this).parent().toggleClass('active');
                $(this).next().slideToggle(300);

            });

            console.log(itemPrices[0]);
            searchBtn.addEventListener('click', function () {
                if (from.value > itemPrices[0]) {
                    console.log('Правильно')
                }
                else {
                    console.log('Не правильно')
                }
            })
        }
        priceFilter();
    })

    function renderPage(page, items) {
        if (items.length != 0) {
            content.innerHTML = '';
        }
        let begin;
        if (page == 0) {
            begin = 0;
        }
        else {
            begin = (page - 1) * perPage;
        }
        for (let i = begin; i < begin + perPage; i++) {
            if (items[i]) {
                content.appendChild(renderItem(items[i]));
            }
            else {
                break;
            }
        }
        colorSlider();
    }

    function colorSlider() {
        document.querySelectorAll('.item__colors .fa-circle').forEach(circle => circle.addEventListener('click', function () {
            $(`img[data-id=${this.getAttribute('data-id')}]`)
                .fadeOut(400, function () {
                    $(`img[data-id=${this.getAttribute('data-id')}]`).attr('src', circle.getAttribute('data-src'));
                })
                .fadeIn(400);
        }));
    }

    function renderAllItems(items) {
        items = items;
        renderPage(0, items);
        if (items.length) {
            if (pagination.firstChild) {
                pagination.removeChild(pagination.firstChild);
            }
            pagination.appendChild(renderPagination());
            let paginationLinks = document.querySelectorAll('.page-link');
            paginationLinks[0].parentElement.classList.add('disabled');
            paginationLinks.forEach(link => {
                link.addEventListener('click', function () {
                    renderPage(parseInt(link.outerText), items);
                    let siblings = getSiblings(link.parentElement);
                    siblings.forEach(sibling => {
                        sibling.classList.remove('disabled');
                    });
                    link.parentElement.classList.add('disabled');
                });
            });
        }
    }

    let quantityTarget = document.querySelectorAll('.quantity-target');
    quantityTarget.forEach(target => target.addEventListener('click', function () {
        target.classList.add('active-num');
        $(this).siblings().removeClass('active-num');
        if (items.length >= +target.outerText) {
            perPage = +target.outerText;
        }
        else {
            perPage = items.length;
        }
        renderAllItems(items);
    }));

    axios.get('./items/catalog.json')
        .then(catalog => {
            items = catalog.data.items;
            renderAllItems(items);
        });





    function pagesAmount() {
        return Math.ceil(items.length / perPage);
    }

    let getSiblings = function (elem) {
        let siblings = [];
        let sibling = elem.parentNode.firstChild;
        while (sibling) {
            if (sibling.nodeType === 1 && sibling !== elem) {
                siblings.push(sibling);
            }
            sibling = sibling.nextSibling
        }
        return siblings;
    };

    function renderPagination() {
        let ul = document.createElement('ul');
        ul.classList.add('pagination');
        for (let i = 1; i <= pagesAmount(); i++) {
            let li = document.createElement('li');
            li.classList.add('page-item');
            let a = document.createElement('a');
            a.classList.add('page-link');
            a.href = "#catalog__body-container_items"
            a.innerText = i;
            li.appendChild(a);
            ul.appendChild(li);
        }
        return ul;
    }

}
else if ($('div').is('.personal')) {
    $(document.querySelectorAll('.ssize, .csize, .birthday, .birthmonth, .birthyear, .porch, .floor, .room, .door')).select2({
        minimumResultsForSearch: Infinity,
        width: '100%'
    });
    let form = document.forms.personal__form
    form.addEventListener('submit', function(e) {
        e.preventDefault();
        let formData = new FormData(form)
        console.log(...formData)
        // axios.get("url", formData)
        // .then(() => {
        //     // ...
        // })
        // .catch(error => {
        //     // ...
        // });
    });
    let moreBtn = document.getElementsByClassName('add-more')[0];
    let moreContainer = document.getElementById('more-container');
    moreBtn.addEventListener('click', function() {
        let form__group = []
        form__group[0] = document.createElement('div');
        form__group[0].classList.add('form-group');
        form__group[0].appendChild(document.createElement('label'));
        form__group[0].getElementsByTagName('label')[0].setAttribute('for', 'city2');
        form__group[0].getElementsByTagName('label')[0].innerText = 'Город';
        form__group[0].appendChild(document.createElement('input'));
        form__group[0].getElementsByTagName('input')[0].setAttribute('name', 'city2');
        form__group[0].getElementsByTagName('input')[0].setAttribute('type', 'text');
        form__group[0].getElementsByTagName('input')[0].classList.add('form-control');
        form__group[0].getElementsByTagName('input')[0].setAttribute('placeholder', 'Алматы');
        form__group[0].getElementsByTagName('input')[0].id = 'city2';

        form__group[1] = document.createElement('div');
        form__group[1].classList.add('form-group');
        form__group[1].appendChild(document.createElement('label'));
        form__group[1].getElementsByTagName('label')[0].setAttribute('for', 'street2');
        form__group[1].getElementsByTagName('label')[0].innerText = 'Улица';
        form__group[1].appendChild(document.createElement('input'));
        form__group[1].getElementsByTagName('input')[0].setAttribute('name', 'street2');
        form__group[1].getElementsByTagName('input')[0].setAttribute('type', 'text');
        form__group[1].getElementsByTagName('input')[0].classList.add('form-control');
        form__group[1].getElementsByTagName('input')[0].setAttribute('placeholder', 'Торайгырова');
        form__group[1].getElementsByTagName('input')[0].id = 'street2';

        form__group[2] = document.createElement('div');
        form__group[2].classList.add('form-group', 'justify-content-end');
        form__group[2].appendChild(document.createElement('div'));
        form__group[2].getElementsByTagName('div')[0].classList.add('select-wrapper', 'address');
        for (let i = 0; i < 2; i++) {
            form__group[2].getElementsByTagName('div')[0].appendChild(document.createElement('div'));
            form__group[2].getElementsByTagName('div')[0].getElementsByTagName('div')[i].classList.add('address-wrapper');
            form__group[2].getElementsByTagName('div')[0].getElementsByTagName('div')[i].appendChild(document.createElement('label'));
            form__group[2].getElementsByTagName('div')[0].getElementsByTagName('div')[i].appendChild(document.createElement('input'));
        }
        form__group[2].getElementsByTagName('div')[0].getElementsByTagName('div')[0].getElementsByTagName('label')[0].setAttribute('for', 'door2');
        form__group[2].getElementsByTagName('div')[0].getElementsByTagName('div')[0].getElementsByTagName('input')[0].id = 'door2';
        form__group[2].getElementsByTagName('div')[0].getElementsByTagName('div')[0].getElementsByTagName('input')[0].setAttribute('type', 'text');
        form__group[2].getElementsByTagName('div')[0].getElementsByTagName('div')[0].getElementsByTagName('input')[0].setAttribute('name', 'door2');
        form__group[2].getElementsByTagName('div')[0].getElementsByTagName('div')[0].getElementsByTagName('input')[0].setAttribute('placeholder', '4');
        form__group[2].getElementsByTagName('div')[0].getElementsByTagName('div')[0].getElementsByTagName('input')[0].classList.add('form-control');

        form__group[2].getElementsByTagName('div')[0].getElementsByTagName('div')[1].getElementsByTagName('label')[0].setAttribute('for', 'room2');
        form__group[2].getElementsByTagName('div')[0].getElementsByTagName('div')[1].getElementsByTagName('input')[0].id = 'room2';
        form__group[2].getElementsByTagName('div')[0].getElementsByTagName('div')[1].getElementsByTagName('input')[0].setAttribute('type', 'text');
        form__group[2].getElementsByTagName('div')[0].getElementsByTagName('div')[1].getElementsByTagName('input')[0].setAttribute('name', 'room2');
        form__group[2].getElementsByTagName('div')[0].getElementsByTagName('div')[1].getElementsByTagName('input')[0].setAttribute('placeholder', '4');
        form__group[2].getElementsByTagName('div')[0].getElementsByTagName('div')[1].getElementsByTagName('input')[0].classList.add('form-control');

        form__group[3] = document.createElement('div');
        form__group[3].classList.add('form-group', 'justify-content-end');
        form__group[3].appendChild(document.createElement('div'));
        form__group[3].getElementsByTagName('div')[0].classList.add('select-wrapper', 'address');
        for (let i = 0; i < 2; i++) {
            form__group[3].getElementsByTagName('div')[0].appendChild(document.createElement('div'));
            form__group[3].getElementsByTagName('div')[0].getElementsByTagName('div')[i].classList.add('address-wrapper');
            form__group[3].getElementsByTagName('div')[0].getElementsByTagName('div')[i].appendChild(document.createElement('label'));
            form__group[3].getElementsByTagName('div')[0].getElementsByTagName('div')[i].appendChild(document.createElement('input'));
        }
        form__group[3].getElementsByTagName('div')[0].getElementsByTagName('div')[0].getElementsByTagName('label')[0].setAttribute('for', 'porch2');
        form__group[3].getElementsByTagName('div')[0].getElementsByTagName('div')[0].getElementsByTagName('input')[0].id = 'porch2';
        form__group[3].getElementsByTagName('div')[0].getElementsByTagName('div')[0].getElementsByTagName('input')[0].setAttribute('type', 'text');
        form__group[3].getElementsByTagName('div')[0].getElementsByTagName('div')[0].getElementsByTagName('input')[0].setAttribute('name', 'porch2');
        form__group[3].getElementsByTagName('div')[0].getElementsByTagName('div')[0].getElementsByTagName('input')[0].setAttribute('placeholder', '4');
        form__group[3].getElementsByTagName('div')[0].getElementsByTagName('div')[0].getElementsByTagName('input')[0].classList.add('form-control');

        form__group[3].getElementsByTagName('div')[0].getElementsByTagName('div')[1].getElementsByTagName('label')[0].setAttribute('for', 'floor2');
        form__group[3].getElementsByTagName('div')[0].getElementsByTagName('div')[1].getElementsByTagName('input')[0].id = 'floor2';
        form__group[3].getElementsByTagName('div')[0].getElementsByTagName('div')[1].getElementsByTagName('input')[0].setAttribute('type', 'text');
        form__group[3].getElementsByTagName('div')[0].getElementsByTagName('div')[1].getElementsByTagName('input')[0].setAttribute('name', 'floor2');
        form__group[3].getElementsByTagName('div')[0].getElementsByTagName('div')[1].getElementsByTagName('input')[0].setAttribute('placeholder', '4');
        form__group[3].getElementsByTagName('div')[0].getElementsByTagName('div')[1].getElementsByTagName('input')[0].classList.add('form-control');
        for (let i = 0; i < 4; i++) {
            moreContainer.appendChild(form__group[i]);
        }
        this.style.display = 'none';
    });
}